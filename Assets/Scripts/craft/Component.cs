using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using StarterAssets;
using Photon.Realtime;

public class Component : MonoBehaviourPunCallbacks
{
    Inventory inventory;

    public enum TypeComponents
    {
        copper,
        metal,
        electronic,
        organic,
        exploPowder,
    };

    public TypeComponents typeComposant;
    
    public int quantity;
    public int rarity;


    // Start is called before the first frame update
    void Start()
    {
        Renderer objectRenderer = this.gameObject.GetComponent<Renderer>();


        switch (typeComposant)
        {
            case TypeComponents.copper:
                objectRenderer.material.color = Color.red;
                break;
            case TypeComponents.metal:
                objectRenderer.material.color = Color.grey;
                break;
            case TypeComponents.electronic:
                objectRenderer.material.color = Color.blue;
                break;
            case TypeComponents.organic:
                objectRenderer.material.color = Color.white;
                break;
            case TypeComponents.exploPowder:
                objectRenderer.material.color = Color.black;
                break;
            default:
                break;
        }
    }

 
    public TypeComponents GetComponentType()
    {
        return typeComposant;
    }

    

    [PunRPC]
    public void TakeComponentRPC()
    {

        // Désactiver l'objet dans le monde
        gameObject.SetActive(false);


    }


}
