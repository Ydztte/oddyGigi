using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using StarterAssets;
using Photon.Pun;

public class ConsoleGameController : MonoBehaviourPunCallbacks

{
    public TextMeshProUGUI textTypeConsole;
    public TextMeshProUGUI textCompoNeeded;
    public Image imgTest;

    public TextMeshProUGUI textTypeConsoleCanvas;
    public TextMeshProUGUI textCompoNeededCanvas;

    public GameObject PanelCanvasPrincipal;

    public Button addCopper;
    public Button addMetal;
    public Button addElect;
    public Button addOrga;
    public Button addExpPow;
    
    public Button removeCopper;
    public Button removeMetal;
    public Button removeElect;
    public Button removeOrga;
    public Button removeExpPow;


    public enum TypeConsole
    {
        navigation,
        sheild,
        weapons,
        machine,
        labo,
        botanical,
        communication,
    };

    [SerializeField]
    private int copperNeeded;
    [SerializeField]
    private int metalNeeded;
    [SerializeField]
    private int electronicNeeded;
    [SerializeField]
    private int organicNeeded;
    [SerializeField]
    private int exploPowderNeeded;


    public TypeConsole typeConsole;



    // Start is called before the first frame update
    void Start()
    {
        


        copperNeeded = Random.Range(1, 2);
        metalNeeded = Random.Range(0, 2);
        electronicNeeded = Random.Range(0, 2);
        organicNeeded = Random.Range(0, 2);
        exploPowderNeeded = Random.Range(0, 2);
            
        UpdateUi();
        

    }

    public void UpdateUi()
    {

        textTypeConsole.text = "" + typeConsole;
        textTypeConsoleCanvas.text = "" + typeConsole;

        textCompoNeeded.text = copperNeeded + " C / "
            + metalNeeded + " M / "
            + electronicNeeded + " E / "
            + organicNeeded + " O / "
            + exploPowderNeeded + " EP";



        textCompoNeededCanvas.text = copperNeeded + " C / "
            + metalNeeded + " M / "
            + electronicNeeded + " E / "
            + organicNeeded + " O / "
            + exploPowderNeeded + " EP";
    }


    private void OnTriggerEnter(Collider other)
    {
        imgTest.gameObject.SetActive(true);

    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(other.GetComponent<ThirdPersonController>().pickupKey))
        {
            PanelCanvasPrincipal.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            PanelCanvasPrincipal.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        imgTest.gameObject.SetActive(false);
    }


    public void AddCopperInConcole(bool add)
    {
        GameObject currentPlayer = GameObject.FindGameObjectWithTag("Player");

        Debug.Log(currentPlayer);

        if (currentPlayer != null)
        {
            if (add)
            {
                currentPlayer.GetComponent<Inventory>().AddComponentCopper();
                copperNeeded--;
                UpdateUi();


                //copper need en rpc !

            }
            else
            {
                currentPlayer.GetComponent<Inventory>().RemoveComponentCopper();
                copperNeeded++;
                UpdateUi();
            }
        }

       
    }
}
