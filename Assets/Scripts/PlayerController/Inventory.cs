using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;
using System.Text;

public class Inventory : MonoBehaviourPunCallbacks
{
    [SerializeField] 
    private List<Component.TypeComponents> components = new List<Component.TypeComponents>();

    [SerializeField]
    private int copper;
    [SerializeField]
    private int metal;
    [SerializeField]
    private int electronic;
    [SerializeField]
    private int organic;
    [SerializeField]
    private int exploPowder;


    // Custom property to synchronize inventory for the local player only
    private List<Component.TypeComponents> InventoryList
    {
        get
        {
            return components;
        }
        set
        {
            components = value;
        }
    }


    private TextMeshProUGUI inventoryText;
    PhotonView PV;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    private void Start()
    {
        UpdateInventoryText();
    }

    public void UpdateInventoryText()
{
    if (!PV.IsMine)
    {
        return; // Only update the inventory of the local player
    }

    // Get the inventory text component from the canvas
    TextMeshProUGUI inventoryText = GameObject.Find("Canvas/InventoryText").GetComponent<TextMeshProUGUI>();

    inventoryText.text = copper + " copper\n"
            + metal + " metal\n"
            + electronic + " electronic\n"
            + organic + " organic\n"
            + exploPowder + " " +
            "exploPowder";
}


    public void AddComponentCopper()
    {
        
        Debug.Log("+1 copper");
        copper++;
        UpdateInventoryText();
    }
    public void AddComponentMetal()
    {
        Debug.Log("+1 metal");
        metal++;
        UpdateInventoryText();
    }
    public void AddComponentElectronic()
    {
        Debug.Log("+1 elect");
        electronic++;
        UpdateInventoryText();
    }
    public void AddComponentOrganic()
    {
        Debug.Log("+1 orga");
        organic++;
        UpdateInventoryText();
    }
    public void AddComponentExploPowder()
    {
        Debug.Log("+1 exploP");
        exploPowder++;
        UpdateInventoryText();
    }
    
    
    
    public void RemoveComponentCopper()
    {
        
        Debug.Log("-1 copper");
        copper--;
        UpdateInventoryText();
    }
    public void RemoveComponentMetal()
    {
        Debug.Log("-1 metal");
        metal--;
        UpdateInventoryText();
    }
    public void RemoveComponentElectronic()
    {
        Debug.Log("-1 elect");
        electronic--;
        UpdateInventoryText();
    }
    public void RemoveComponentOrganic()
    {
        Debug.Log("-1 orga");
        organic--;
        UpdateInventoryText();
    }
    public void RemoveComponentExploPowder()
    {
        Debug.Log("-1 exploP");
        exploPowder--;
        UpdateInventoryText();
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        if (targetPlayer == PhotonNetwork.LocalPlayer)
        {
            if (changedProps.ContainsKey("inventory"))
            {
                components = (List<Component.TypeComponents>)changedProps["inventory"];
            }
        }
    }


    /*
    public Component GetItemByName(Component compo)
    {
        return items.Find(item => item.name == name);
    }*/
}
