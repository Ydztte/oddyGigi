using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class PlayerManager : MonoBehaviour
{
    PhotonView PV;


    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PV.IsMine)
        {
            CreateController();
        }
    }


    void CreateController()
    {
        Debug.Log("instantiate player");
        //cr�ation du player

        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Player&Cam"), Vector3.zero, Quaternion.identity);


    }
}
